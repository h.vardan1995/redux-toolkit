import "./App.scss";
import { BrowserRouter as Router } from "react-router-dom";
import Todo from "./components/todoAdd/Todo";
import TodoList from "./components/todoList/TodoList";

import RootRouter from "./RootRouter";
function App() {
  return (
    <Router>
      <div className="App">
        <RootRouter />
      </div>
    </Router>
  );
}

export default App;
