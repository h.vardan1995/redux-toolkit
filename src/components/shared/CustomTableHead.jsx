import React from "react";

const CustomTableHead = ({ columns = [] }) => {
  const items = columns.map(({ key, title }) => {
    return (
      <th style={{ borderTop: "none" }} key={key}>
        {title}
      </th>
    );
  });
  return (
    <thead>
      <tr style={{ color: "#878FA1" }}>{items}</tr>
    </thead>
  );
};

export default React.memo(CustomTableHead);
