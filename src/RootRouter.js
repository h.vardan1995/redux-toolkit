import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Todo from "./components/todoAdd/Todo";
import TodoList from "./components/todoList/TodoList";

const RootRouter = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <TodoList />
        </Route>
        <Route exact path="/todo/add">
          <Todo />
        </Route>
        <Route exact path="/todo/edit/:id">
          <Todo />
        </Route>
        <Redirect to="/" />
      </Switch>
    </div>
  );
};

export default RootRouter;
