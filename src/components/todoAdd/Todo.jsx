import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { todosAdd, todosUpdate } from "../../store/todosSlice";

const Todo = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const history = useHistory();

  const [todo, setTodo] = useState({
    id,
    title: "",
    completed: false,
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    if (id) {
      dispatch(todosUpdate(id, todo));
    } else {
      dispatch(todosAdd(todo));
      history.replace("/");
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Enter a task"
          onChange={(e) => setTodo({ ...todo, title: e.target.value })}
        />

        <button>{id ? "Edit" : "Add"}</button>
      </form>
    </>
  );
};

export default Todo;
