import { Button } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { todosGet } from "../../store/todosSlice";
import CustomTableBody from "../shared/CustomTableBody";
import CustomTableHead from "../shared/CustomTableHead";
import { Link, useHistory } from "react-router-dom";

const TodoList = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const todoState = useSelector((state) => state.todosState);
  const { todos } = todoState;

  useEffect(() => {
    dispatch(todosGet());
  }, [dispatch, history]);

  return (
    <>
      <Button component={Link} to="/todo/add" variant="contained">
        Add new task
      </Button>
      {todos ? (
        <table className="table table-striped">
          <CustomTableHead
            columns={[
              { key: "id", title: "№" },
              { key: "title", title: "Title" },
              { key: "completed", title: "Status" },
              { key: "actions", title: "Actions" },
            ]}
          />

          <CustomTableBody
            tableColumns={todos}
            renderColumns={(column) => (
              <>
                <td>{column.id ? column.id : "-"}</td>
                <td>{column.title ? column.title : "-"}</td>
                <td>
                  {column.completed ? (
                    <i
                      className="fas fa-check-circle ml-3"
                      style={{ color: "green" }}
                    ></i>
                  ) : (
                    <i
                      className="fas fa-times-circle  ml-3"
                      style={{ color: "red" }}
                    ></i>
                  )}
                </td>
                <td>
                  <span className="ml-2">
                    <i
                      class="fas fa-pencil-alt"
                      onClick={() => {
                        history.push(`/todo/edit/${column.id}`);
                      }}
                    ></i>
                  </span>
                  <span className="ml-3">
                    <i
                      className="fas fa-trash-alt"
                      //   onClick={() => {
                      //     dispatch(todosDelete(column.id));
                      //   }}
                    ></i>
                  </span>
                </td>
              </>
            )}
          />
        </table>
      ) : (
        "loading..."
      )}
    </>
  );
};

export default TodoList;
