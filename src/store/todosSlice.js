import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import cogoToast from "rb-cogo-toast";

const baseURL = "https://jsonplaceholder.typicode.com/";

const initialState = {
  todos: [],
  addtodoStatus: "",
  addtodoError: "",
  gettodosStatus: "",
  gettodosError: "",
  updatetodoStatus: "",
  updatetodoError: "",
  deletetodoStatus: "",
  deletetodoError: "",
};

export const todosAdd = createAsyncThunk(
  "TODOS/TODOSADD",
  async (todo, { rejecteWithValue }) => {
    try {
      const response = await axios.post(baseURL + "todos", todo);
      cogoToast.success("New Post was added");

      return response.data;
    } catch (error) {
      cogoToast.error("Something went wrong!");
      return rejecteWithValue(error.response.data);
    }
  }
);

export const todosGet = createAsyncThunk(
  "TODOS/TODOSGET",
  async (id = null, { rejecteWithValue }) => {
    try {
      const response = await axios.get(baseURL + "todos?_page=1&_limit=8");
      return response.data;
    } catch (error) {
      console.log(error);
      cogoToast.error("Something went wrong!");
      return rejecteWithValue(error.response.data);
    }
  }
);

export const todosUpdate = createAsyncThunk(
  "TODOS/TODOSUPDATE",
  async (todo, { rejecteWithValue }) => {
    const { id, title } = todo;
    try {
      const response = await axios.update(baseURL + "todos/" + id, title);
      cogoToast.success("Updated");
      return response.data;
    } catch (error) {
      console.log(error);
      cogoToast.error("Something went wrong!");
      return rejecteWithValue(error.response.data);
    }
  }
);

// export const todosDelete = createAsyncThunk(
//   "TODOS/TODOSDELETE",
//   async (id = null, { rejecteWithValue }) => {
//     try {
//       const response = await axios.delete(baseURL + `todos/${id}`, id);
//       cogoToast.success("Deleted");
//       return response.data;
//     } catch (error) {
//       console.log(error);
//       cogoToast.error("Something went wrong!");
//       return rejecteWithValue(error.response.data);
//     }
//   }
// );

const todosSlice = createSlice({
  name: "TODOS",
  initialState,
  reducers: {},
  extraReducers: {
    [todosAdd.pending]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "pending",
        addtodoError: "",
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosAdd.fulfilled]: (state, action) => {
      return {
        ...state,
        todos: [action.payload, ...state.todos],
        addtodoStatus: "success",
        addtodoError: "",
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosAdd.rejected]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "rejected",
        addtodoError: action.payload,
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },

    [todosGet.pending]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "pending",
        gettodosError: "",
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosGet.fulfilled]: (state, action) => {
      return {
        ...state,
        todos: action.payload,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "success",
        gettodosError: "",
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosGet.rejected]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "rejected",
        gettodosError: action.payload,
        updatetodoStatus: "",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },

    [todosUpdate.pending]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "pending",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosUpdate.fulfilled]: (state, action) => {
      const updatedTodos = state.todos.map((todo) =>
        todo.id === action.payload.id ? action.payload : todo
      );
      return {
        ...state,
        // todos: updatedTodos,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "success",
        updatetodoError: "",
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },
    [todosUpdate.rejected]: (state, action) => {
      return {
        ...state,
        addtodoStatus: "",
        addtodoError: "",
        gettodosStatus: "",
        gettodosError: "",
        updatetodoStatus: "rejected",
        updatetodoError: action.payload,
        deletetodoStatus: "",
        deletetodoError: "",
      };
    },

    // [todosDelete.pending]: (state, action) => {
    //   return {
    //     ...state,
    //     addtodoStatus: "",
    //     addtodoError: "",
    //     gettodosStatus: "",
    //     gettodosError: "",
    //     updatetodoStatus: "",
    //     updatetodoError: "",
    //     deletetodoStatus: "pending",
    //     deletetodoError: "",
    //   };
    // },
    // [todosDelete.fulfilled]: (state, action) => {
    //   return {
    //     ...state,
    //     todos: [action.payload, ...state.todos],
    //     addtodoStatus: "",
    //     addtodoError: "",
    //     gettodosStatus: "",
    //     gettodosError: "",
    //     updatetodoStatus: "",
    //     updatetodoError: "",
    //     deletetodoStatus: "success",
    //     deletetodoError: "",
    //   };
    // },
    // [todosDelete.rejected]: (state, action) => {
    //   return {
    //     ...state,
    //     addtodoStatus: "",
    //     addtodoError: "",
    //     gettodosStatus: "",
    //     gettodosError: "",
    //     updatetodoStatus: "",
    //     updatetodoError: "",
    //     deletetodoStatus: "rejected",
    //     deletetodoError: action.payload,
    //   };
    // },
  },
});

export default todosSlice.reducer;
